CREATE TABLE Persona (
  id serial primary key,
  RUT VARCHAR(10) NOT NULL UNIQUE,
  Nombres VARCHAR(120) NOT NULL,
  Apellido_paterno VARCHAR(120) NOT NULL,
  Apellido_materno VARCHAR(120) NOT NULL,
  Email VARCHAR(100) NOT NULL UNIQUE,
  Telefono VARCHAR(120) NULL DEFAULT '',
  Fecha_nacimiento DATE NOT NULL,
  Fecha_ingreso DATE NOT NULL DEFAULT now(),
  Estatus CHAR(1) NOT NULL DEFAULT 'A'
);

CREATE TABLE Profesor (
  id  serial primary key,
  id_Persona INTEGER NOT NULL,
  Cargo VARCHAR(50) NOT NULL DEFAULT ''
);

CREATE TABLE Alumno (
  id  serial primary key,
  id_Persona INTEGER NOT NULL,
  Comentario TEXT NULL DEFAULT ''
);

CREATE TABLE Curso (
  id  serial primary key,
  Codigo VARCHAR(50) NOT NULL,
  id_Profesor INTEGER NOT NULL,
  Nombre VARCHAR(100) NOT NULL,
  Descripcion TEXT NULL DEFAULT '',
  Fecha_inicio DATE NOT NULL DEFAULT now(),
  Max_nota INTEGER NOT NULL DEFAULT 10,
  Estatus CHAR(1) NOT NULL DEFAULT 'A'
);

CREATE TABLE Prueba (
  id serial primary key,
  Nombre VARCHAR(100) NOT NULL,
  Descripcion TEXT NULL DEFAULT ''
);

CREATE TABLE Alumno_Curso (
  id serial primary key,
  id_Alumno INTEGER NOT NULL,
  id_Curso INTEGER NOT NULL
);

CREATE TABLE Resultado_Prueba (
  id  serial primary key,
  id_Alumno_Curso INTEGER NOT NULL,
  id_Prueba INTEGER NOT NULL,
  Fecha DATE NOT NULL DEFAULT now(),
  Nota INTEGER NOT NULL DEFAULT 0,
  Estatus CHAR NOT NULL DEFAULT 'A'
);

ALTER TABLE Alumno_Curso ADD FOREIGN KEY (id_Alumno) REFERENCES Alumno (id);
ALTER TABLE Alumno_Curso ADD FOREIGN KEY (id_Curso) REFERENCES Curso (id);
ALTER TABLE Alumno ADD FOREIGN KEY (id_Persona) REFERENCES Persona (id);
ALTER TABLE Profesor ADD FOREIGN KEY (id_Persona) REFERENCES Persona (id);
ALTER TABLE Curso ADD FOREIGN KEY (id_Profesor) REFERENCES Profesor (id);
ALTER TABLE Resultado_Prueba ADD FOREIGN KEY (id_Alumno_Curso) REFERENCES Alumno_Curso (id);
ALTER TABLE Resultado_Prueba ADD FOREIGN KEY (id_Prueba) REFERENCES Prueba (id);
