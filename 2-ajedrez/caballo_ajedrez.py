from operator import itemgetter
from random import random

movimientos = ((2, 1), (2, -1), (1, 2), (1, -2),
               (-2, 1), (-2, -1), (-1, 2), (-1, -2))


def __validar_rango(n, tablero, pos_x, pos_y):
    return ((0 <= pos_x < n) and
            (0 <= pos_y < n) and
            tablero[pos_x][pos_y] == 0)


def __disponibilidad(n, tablero, x, y):
    return sum(1 for deltax, deltay in movimientos if __validar_rango(n, tablero, x + deltax, y + deltay))


def __genera_movs(positionx, positiony):
    for deltax, deltay in movimientos:
        yield positionx + deltax, positiony + deltay


def __proximo_movimiento(n, tablero, move):
    posi_x, posi_y = move
    movimientos_posibles = [
        ((x, y), __disponibilidad(n, tablero, x, y))
        for x, y in __genera_movs(posi_x, posi_y)
        if __validar_rango(n, tablero, x, y)
        ]
    if len(movimientos_posibles) == 0:
        return None
    nuevo_mov = sorted(movimientos_posibles, key=itemgetter(1))[0]
    return nuevo_mov[0]


def caballo_ajedrez(num):
    # Se valida contra 6 porque menor que esa cantidad no hay solucion posible y 100 supera el tiempo de respuesta
    if type(num) is not int or num < 6 or num > 100:
        print '{0} no es un numero valido'.format(num)
        return None

    tablero = [[0] * num for x in range(num)]
    pos_x = int(random() * num)
    pos_y = int(random() * num)
    tablero[pos_x][pos_y] = 1
    respuesta = [[pos_x, pos_y]]

    for numero_movimiento in range(2, num * num + 1):
        movimiento = pos_x, pos_y
        pos_x, pos_y = __proximo_movimiento(num, tablero, movimiento)
        respuesta.append([pos_x, pos_y])
        tablero[pos_x][pos_y] = 1

    return respuesta
