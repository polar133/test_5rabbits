from unittest import TestCase
import sys
from numeros_palabras import numeros_palabras


class TestNumerosPalabras(TestCase):

    def test_validar_numero(self):
        respuesta = numeros_palabras("No es un numero")
        self.assertEqual(respuesta, None)

    def test_validar_numero_positivo(self):
        respuesta = numeros_palabras(-1574)
        self.assertEqual(respuesta, None)

    def test_validar_numeros_enteros(self):
        respuesta = numeros_palabras(12.2541)
        self.assertEqual(respuesta, None)

    def test_validar_mas_de_maximo_entero(self):
        respuesta = numeros_palabras(sys.maxint + 10)
        self.assertEqual(respuesta, None)

    def test_validar_cero(self):
        respuesta = numeros_palabras(0)
        self.assertEqual(respuesta, "Cero")

    def test_validar_unidad(self):
        respuesta = numeros_palabras(5)
        self.assertEqual(respuesta, "Cinco")

    def test_validar_diez(self):
        respuesta = numeros_palabras(10)
        self.assertEqual(respuesta, "Diez")

    def test_validar_quince(self):
        respuesta = numeros_palabras(15)
        self.assertEqual(respuesta, "Quince")

    def test_validar_29(self):
        respuesta = numeros_palabras(29)
        self.assertEqual(respuesta, "Veintinueve")

    def test_validar_cuarenta(self):
        respuesta = numeros_palabras(40)
        self.assertEqual(respuesta, "Cuarenta")

    def test_validar_sesenta_y_siete(self):
        respuesta = numeros_palabras(67)
        self.assertEqual(respuesta, "Sesenta y Siete")

    def test_validar_145(self):
        respuesta = numeros_palabras(145)
        self.assertEqual(respuesta, "Ciento Cuarenta y Cinco")

    def test_validar_1000000(self):
        respuesta = numeros_palabras(1000000)
        self.assertEqual(respuesta, "Un Millon")

    def test_validar_5000000(self):
        respuesta = numeros_palabras(5000000)
        self.assertEqual(respuesta, "Cinco Millones")

    def test_validar_147483647(self):
        respuesta = numeros_palabras(147483647)
        self.assertEqual(respuesta, "Ciento Cuarenta y Siete Millones Cuatrocientos Ochenta y Tres Mil Seiscientos Cuarenta y Siete")

    def test_validar_maximo(self):
        respuesta = numeros_palabras(sys.maxint)
        self.assertEqual(respuesta, "Dos Billones Ciento Cuarenta y Siete Millones Cuatrocientos Ochenta y Tres Mil Seiscientos Cuarenta y Siete")

