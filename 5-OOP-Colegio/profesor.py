# -*- coding: utf-8 -*-
from datetime import datetime
import curso
import persona


class Profesor(persona.Persona):
    def __init__(self, cargo="", *args, **kwargs):
        persona.Persona.__init__(self, *args, **kwargs)
        self.__cargo = cargo
        self.cursos = {}

    # Getter of cargo
    @property
    def cargo(self):
        return self.__cargo

    # Setter of cargo
    @cargo.setter
    def cargo(self, cargo):
        self.__cargo = cargo

    def asignar_curso(self, p_curso):
        self.cursos[p_curso.codigo] = p_curso

    def a_cargo_de_curso(self, codigo_curso):
        if codigo_curso in self.cursos:
            return True
        return False

    def remover_curso(self, codigo_curso):
        self.cursos.pop(codigo_curso)

    def __str__(self):
        return "{0} {1} {2}".format(self.nombres, self.apellido_paterno, self.apellido_materno)
