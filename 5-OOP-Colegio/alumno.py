# -*- coding: utf-8 -*-
import curso
import persona


class Alumno(persona.Persona):
    def __init__(self, comentario="", *args, **kwargs):
        persona.Persona.__init__(self, *args, **kwargs)
        self.__comentario = comentario
        self.__cursos = []

    # Getter of comentario
    @property
    def comentario(self):
        return self.__comentario

    # Setter of comentario
    @comentario.setter
    def comentario(self, comentario):
        self.__comentario = comentario

    @property
    def cursos(self):
        return self.__cursos

    def inscribir_curso(self, p_curso):
        """
        Inscribir alumno en curso indicado
        :param p_curso: Objeto Curso
        :return:
        """
        if type(p_curso) is curso.Curso and p_curso not in self.cursos:
            self.cursos.append(p_curso)
            p_curso.agregar_alumno(self)

    def retirar_curso(self, p_curso):
        """
        Retirar alumno del curso indicado si existe relacion
        :param p_curso: Objeto Curso
        :return:
        """
        if type(p_curso) is curso.Curso and p_curso in self.cursos:
            self.cursos.remove(p_curso)
            p_curso.retirar_alumno(self)

    def cantidad_cursos(self):
        """
        Cantidad de cursos de alumno
        :return:
        """
        return len(self.cursos)

    def promedio(self, p_curso):
        """
        Promedio de notas del alumno en curso
        :param p_curso: Objeto Curso
        :return:
        """
        if type(p_curso) is curso.Curso:
            notas = p_curso.notas(self)
            if notas:
                return sum(notas)/len(notas)
        return None

    def __str__(self):
        return "{0} {1} {2}".format(self.nombres, self.apellido_materno, self.apellido_paterno)
