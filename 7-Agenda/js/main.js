function renderCalendar() {
    var days = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
    var start_time = 7;
    var end_time = 20;

    $("#calendar").append(
        "<div class='row'> \
            <div class='block time header'><label>#</label></div> \
            <div class='block header'><label>" + days[0] + "</label></div> \
            <div class='block header'><label>" + days[1] + "</label></div> \
            <div class='block header'><label>" + days[2] + "</label></div> \
            <div class='block header'><label>" + days[3] + "</label></div> \
            <div class='block header'><label>" + days[4] + "</label></div> \
            <div class='block header'><label>" + days[5] + "</label></div> \
            <div class='block header'><label>" + days[6] + "</label></div> \
        </div>");

    for (var i = start_time; i <= end_time; i++) {
        for (var j = 0; j <= 1; j++) {
            var time = i.toString();
            var minutes = (j == 0 ? ":00" : ":30");
            var id_subfix = time + "_" + j

            $("#calendar").append(
                "<div class='row'> \
                    <div class='block time'><label>" + time + minutes + "</label></div> \
                    <div id=" + 'lunes_' + id_subfix + " class='block'></div> \
                    <div id=" + 'martes_' + id_subfix + " class='block'></div> \
                    <div id=" + 'miercoles_' + id_subfix + " class='block'></div> \
                    <div id=" + 'jueves_' + id_subfix + " class='block'></div> \
                    <div id=" + 'viernes_' + id_subfix + " class='block'></div> \
                    <div id=" + 'sabado_' + id_subfix + " class='block'></div> \
                    <div id=" + 'domingo_' + id_subfix + " class='block'></div> \
                </div>");
        };
    };


    $.getJSON("js/dummy_data/data.json", function (jsonObject) {
        var count_elements = 0;
        for (var element in jsonObject) {
            var id_block_prefix = element + "_";
            for (var i = 0; i < jsonObject[element].length; i++) {
                var start_time_array = jsonObject[element][i].hora_inicio.split(":");
                var end_time_array = jsonObject[element][i].hora_termino.split(":");

                var start_hour = parseInt(start_time_array[0]);
                var end_hour = parseInt(end_time_array[0]);
                var subfix_start_time = (start_time_array[1] == "00" ? 0 : 1);
                var subfix_end_time = (end_time_array[1] == "00" ? 0 : 1);

                var amount_blocks = ((end_hour - start_hour) * 2) - subfix_start_time + subfix_end_time;

                var pivot_time = start_hour;
                var pivot_minutes = subfix_start_time;

                for (var j = 0; j < amount_blocks; j++) {
                    var id_block = id_block_prefix + pivot_time + "_" + pivot_minutes;
                    if (pivot_minutes == 0)
                        pivot_minutes = 1;
                    else {
                        pivot_time += 1;
                        pivot_minutes = 0;
                    }
                    if (count_elements % 2 == 0)
                        $('#' + id_block).addClass("used-light");
                    else
                        $('#' + id_block).addClass("used-dark");

                    $('#' + id_block).append('<label>' + jsonObject[element][i].nombre + '</label>');

                }
                count_elements += 1;
            };
        }
    });
};


$(document).ready(renderCalendar);