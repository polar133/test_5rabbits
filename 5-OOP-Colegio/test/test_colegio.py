# -*- coding: utf-8 -*-
from unittest import TestCase
import datetime
import alumno
import curso
import profesor
import prueba


class TestColegio(TestCase):
    def setUp(self):
        self.lionel_messi = alumno.Alumno("", "10000000-1", "Lionel", "Messi", "Whatever", "messi@gmail.com", "1231230", datetime.date(1988, 1, 1))
        self.cristiano_ronaldo = alumno.Alumno("", "20000000-1", "Cristiano", "Ronaldo", "Whatever", "cronaldo@gmail.com", "1231230", datetime.date(1988, 1, 1))
        self.luis_suarez = alumno.Alumno("", "30000000-1", "Luis", "Suárez", "Whatever", "suarez@gmail.com", "1231230", datetime.date(1987, 1, 1))
        self.arjen_robben = alumno.Alumno("", "40000000-1", "Arjen", "Robben", "Whatever", "nofuepenalty@gmail.com", "1231230", datetime.date(1986, 1, 1))
        self.manuel_neuer = alumno.Alumno("", "50000000-1", "Manuel", "Neuer", "Whatever", "neuer@gmail.com", "1231230", datetime.date(1990, 1, 1))

        self.alessandro_nesta = profesor.Profesor("", "60000000-1", "Alessandro", "Nesta", "Whatever", "nesta@gmail.com", "1231230", datetime.date(1976, 1, 1))
        self.george_best = profesor.Profesor("", "70000000-1", "George", "Best", "Whatever", "best@gmail.com", "1231230", datetime.date(1958, 1, 1))

        self.curso_programacion = curso.Curso("0001", "programación", self.george_best)
        self.curso_ataque = curso.Curso("101", "Ataque", self.george_best)
        self.curso_defensa = curso.Curso("102", "Defensa", self.alessandro_nesta)

        self.prueba1 = prueba.Prueba("Prueba1")
        self.prueba2 = prueba.Prueba("Prueba2")
        self.prueba3 = prueba.Prueba("Prueba3")

        self.curso_programacion.agregar_alumno(self.lionel_messi)
        self.curso_programacion.agregar_alumno(self.cristiano_ronaldo)
        self.curso_programacion.agregar_alumno(self.luis_suarez)
        self.curso_programacion.agregar_alumno(self.manuel_neuer)

        self.curso_ataque.agregar_alumno(self.lionel_messi)
        self.curso_ataque.agregar_alumno(self.arjen_robben)
        self.curso_ataque.agregar_alumno(self.manuel_neuer)

        self.curso_defensa.agregar_alumno(self.manuel_neuer)

        self.curso_programacion.agregar_resultado_prueba(self.lionel_messi, self.prueba1, 2.5)
        self.curso_programacion.agregar_resultado_prueba(self.lionel_messi, self.prueba2, 3.5)
        self.curso_programacion.agregar_resultado_prueba(self.lionel_messi, self.prueba3, 7)

        self.curso_programacion.agregar_resultado_prueba(self.cristiano_ronaldo, self.prueba1, 10)
        self.curso_programacion.agregar_resultado_prueba(self.cristiano_ronaldo, self.prueba2, 8)
        self.curso_programacion.agregar_resultado_prueba(self.manuel_neuer, self.prueba2, 7)
        self.curso_programacion.agregar_resultado_prueba(self.manuel_neuer, self.prueba3, 5)

        self.curso_ataque.agregar_resultado_prueba(self.lionel_messi, self.prueba1, 2.5)
        self.curso_defensa.agregar_resultado_prueba(self.manuel_neuer, self.prueba3, 4)

    def test_get_alumnos_curso_programacion(self):
        """
        Prueba lista de alumnos para el curso “programación”
        """
        alumnos_esperados = [self.lionel_messi, self.cristiano_ronaldo, self.luis_suarez, self.manuel_neuer]
        alumnos = self.curso_programacion.alumnos
        self.assertEqual(len(alumnos), len(alumnos_esperados))

        for a, b in zip(alumnos_esperados, alumnos):
            self.assertEqual(a.nombres, b.nombres)

    def test_calcular_promedio_alumno_en_programacion(self):
        """
        Prueba promedio de notas de un alumno en un curso
        """
        promedio_esperado = (2.5+3.5+7)/3
        promedio = self.lionel_messi.promedio(self.curso_programacion)
        self.assertEqual(promedio, promedio_esperado)

    def test_lista_promedio_alumno_por_curso(self):
        """
        Prueba alumnos y el promedio que tiene en cada ramo.
        """
        cursos = [self.curso_programacion, self.curso_ataque, self.curso_defensa]
        lista_esperada = [
            {
                self.lionel_messi.rut: (2.5+3.5+7)/3,
                self.cristiano_ronaldo.rut: 9,
                self.manuel_neuer.rut: 6
            },
            {
                self.lionel_messi.rut: 2.5
            },
            {
                self.manuel_neuer.rut: 4
            }
        ]
        lista_respuesta = []
        for ramo in cursos:
            lista_respuesta.append(ramo.promedio_alumnos())
        self.assertEqual(lista_respuesta, lista_esperada)

    def test_lista_promedio_rojo_en_mas_de_un_curso(self):
        """
        Prueba: lista a todos los alumnos con más de un ramo con promedio rojo
        """
        cursos = [self.curso_programacion, self.curso_ataque, self.curso_defensa]
        lista_esperada = [
            {
                self.lionel_messi.rut: (2.5+3.5+7)/3
            },
            {
                self.lionel_messi.rut: 2.5
            }
        ]
        lista_promedio_bajo = []
        lista_ruts = []
        for ramo in cursos:
            for rut in ramo.promedios_rojos_alumnos().keys():
                lista_ruts.append(rut)
            lista_promedio_bajo.append(ramo.promedios_rojos_alumnos())

        lista_duplicados_ruts = set([x for x in lista_ruts if lista_ruts.count(x) > 1])

        lista_mas_de_promedio_bajo = []
        for promedio_bajo in lista_promedio_bajo:
            for rut, valor in promedio_bajo.iteritems():
                if rut in lista_duplicados_ruts:
                    lista_mas_de_promedio_bajo.append(promedio_bajo)

        self.assertEqual(lista_mas_de_promedio_bajo, lista_esperada)
