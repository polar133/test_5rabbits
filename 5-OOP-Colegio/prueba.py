# -*- coding: utf-8 -*-


class Prueba(object):
    def __init__(self, nombre, desc=""):
        self.__nombre = nombre
        self.__descripcion = desc

    # Getter of nombre
    @property
    def nombre(self):
        return self.__nombre

    # Setter of nombre
    @nombre.setter
    def nombre(self, nombre):
        self.__nombre = nombre

    # Getter of descripcion
    @property
    def descripcion(self):
        return self.__descripcion

    # Setter of descripcion
    @descripcion.setter
    def descripcion(self, desc):
        self.__descripcion = desc

    def __str__(self):
        return self.nombre
