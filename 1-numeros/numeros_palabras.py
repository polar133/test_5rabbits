# -*- coding: utf-8 -*-
import sys


def numeros_palabras(num):
    unidades = ['', 'Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco', 'Seis', 'Siete', 'Ocho', 'Nueve']
    diez_unidad = ['', 'Once', 'Doce', 'Trece', 'Catorce', 'Quince', 'Dieciseis', 'Diecisiete', 'Dieciocho', 'Diecinueve']
    decenas = ['', 'Diez', 'Veinte', 'Treinta', 'Cuarenta', 'Cincuenta', 'Sesenta', 'Setenta', 'Ochenta', 'Noventa']
    veintenas = ['Veinte', 'Veintiuno', 'Veintidos', 'Veintitres', 'Veinticuatro', 'Veinticinco', 'Veintiseis', 'Veintisiete', 'Veintiocho', 'Veintinueve']
    centenas = ['', 'Cien', 'Docientos', 'Trecientos', 'Cuatrocientos', 'Quinientos', 'Seiscientos', 'Setecientos', 'Ochocientos', 'Novecientos']
    miles = ['', 'Mil', 'Millones']
    miles_2 = ['', 'Billón', 'Billones']

    if type(num) is not int or num < 0 or num > sys.maxint:
        print '{0} no es un numero valido'.format(num)
        return None

    palabras = []
    if num == 0:
        palabras.append('Cero')
    else:
        numero_str = str(num)
        numero_str_longitud = len(numero_str)
        groups = (numero_str_longitud+2)/3
        numero_str = numero_str.zfill(groups*3)
        for i in range(0, groups*3, 3):
            h, t, u = int(numero_str[i]), int(numero_str[i+1]), int(numero_str[i+2])
            g = groups-(i/3+1)
            if h >= 1:
                if h == 1 and t == 0 and u == 0:
                    palabras.append(centenas[h])
                else:
                    if h == 1:
                        palabras.append('Ciento')
                    else:
                        palabras.append(centenas[h])
            if t > 2:
                palabras.append(decenas[t])
                if u >= 1:
                    palabras.append('y')
                    palabras.append(unidades[u])
            elif t == 2:
                palabras.append(veintenas[u])
            elif t == 1:
                if u >= 1:
                    palabras.append(diez_unidad[u])
                else:
                    palabras.append(decenas[t])
            else:
                if u >= 1:
                    if (g >= 1) and ((h+t+u) > 0) and u == 1:
                        palabras.append("Un")
                    else:
                        palabras.append(unidades[u])
            if (g >= 1) and ((h+t+u) > 0):
                if h == 0 and t == 0 and u == 1:
                    if g == 3:
                        palabras.append("Billon")
                    else:
                        palabras.append("Millon")
                else:
                    if g == 3:
                        palabras.append("Billones")
                    elif u == 2:
                        palabras.append(miles_2[g])
                    else:
                        palabras.append(miles[g])
    if palabras: 
        return ' '.join(palabras)
    return palabras
