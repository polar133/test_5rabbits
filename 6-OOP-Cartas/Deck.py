# -*- coding: utf-8 -*-
from Card import Card
from random import shuffle


class Deck(object):
    """
    Deck of cards that has multiple Card Objects.
    """
    def __init__(self, cards=[], name=""):
        self._cards = list(cards)
        self._name = name

    # Getter of cards
    @property
    def cards(self):
        return self._cards

    # Setter of cards
    @cards.setter
    def cards(self, cards):
        if type(cards) is list and len(cards) > 0:
            self._cards = cards

    # Getter of name
    @property
    def name(self):
        return self._name

    # Setter of name
    @name.setter
    def name(self, name):
        self._name = name

    def size(self):
        """
        Get length of cards in deck
        :return: integer
        """
        return len(self.cards)

    def add_card(self, card):
        """
        Add a card to deck.
        :param card: Card object
        :return:
        """
        if type(card) is Card:
            self.cards.append(card)

    def pick_card(self):
        """
        Gets the first card in deck. FIFO if isn't shuffle
        :return: Card object or None
        """
        if self.size() > 0:
            return self.cards.pop()
        else:
            return None

    def shuffle(self):
        """
        Shuffle cards in deck
        :return:
        """
        shuffle(self.cards)

    def empty_deck(self):
        """
        Removes all cards in deck
        :return:
        """
        self.cards = []

    def __str__(self):
        if not self.name:
            return "Deck of {0} cards".format(self.size())
        else:
            return self.name
