from unittest import TestCase
import sys
from cantidad_ceros import cantidad_ceros


class TestCantidadCeros(TestCase):

    def test_validar_numero(self):
        respuesta = cantidad_ceros("No es un numero")
        self.assertEqual(respuesta, None)

    def test_validar_numero_positivo(self):
        respuesta = cantidad_ceros(-1574)
        self.assertEqual(respuesta, None)

    def test_validar_numeros_enteros(self):
        respuesta = cantidad_ceros(12.2541)
        self.assertEqual(respuesta, None)

    def test_validar_mas_de_maximo_entero(self):
        respuesta = cantidad_ceros(sys.maxint + 10)
        self.assertEqual(respuesta, None)

    def test_validar_cero(self):
        respuesta = cantidad_ceros(0)
        self.assertEqual(respuesta, 0)

    def test_validar_1808548329(self):
        respuesta = cantidad_ceros(5)
        self.assertEqual(respuesta, 1)

