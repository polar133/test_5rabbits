# -*- coding: utf-8 -*-


class Card(object):
    """
    Card object
    """
    def __init__(self, name, value):
        self._name = name
        self._value = value

    # Getter of name
    @property
    def name(self):
        return self._name

    # Setter of name
    @name.setter
    def name(self, name):
        self._name = name

    # Getter of value
    @property
    def value(self):
        return self._value

    # Setter of value
    @value.setter
    def value(self, value):
        self._value = value

    def __str__(self):
        return self.name+" | "+str(self.value)