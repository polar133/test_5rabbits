from unittest import TestCase
from caballo_ajedrez import caballo_ajedrez as c_a


class TestCaballoAjedrez(TestCase):

    def test_validar_numero(self):
        respuesta = c_a("No es un numero")
        self.assertEqual(respuesta, None)

    def test_validar_numero_positivo(self):
        respuesta = c_a(-1574)
        self.assertEqual(respuesta, None)

    def test_validar_numeros_enteros(self):
        respuesta = c_a(12.2541)
        self.assertEqual(respuesta, None)

    def test_validar_mas_de_1000(self):
        respuesta = c_a(1001)
        self.assertEqual(respuesta, None)

    def test_validar_menos_seis(self):
        respuesta = c_a(0)
        self.assertEqual(respuesta, None)

    def test_caballo_ajedrez(self):
        valor_prueba = 33
        respuesta = c_a(valor_prueba)
        tablero = [[0] * valor_prueba for x in range(valor_prueba)]
        tablero_esperado = [[1] * valor_prueba for x in range(valor_prueba)]
        for casilla in respuesta:
            tablero[casilla[0]][casilla[1]] = 1
        self.assertEqual(tablero, tablero_esperado)
