import sys


def cantidad_ceros(num):
    respuesta = 0
    if type(num) is not int or num < 0 or num > sys.maxint:
        print '{0} no es un numero valido'.format(num)
        return None

    x = 5
    while num >= x:
        respuesta += num / x
        x *= 5

    return respuesta
