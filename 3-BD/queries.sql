--lista de  alumnos para el curso  “programación”
SELECT p.*,
       a.comentario
FROM Persona p,
     Alumno a,
     Alumno_Curso ac,
     Curso c
WHERE c.nombre = 'programación'
  AND ac.id_curso = c.id
  AND ac.id_alumno = a.id
  AND p.id = a.id_persona;

--promedio de notas de un alumno en un  curso
SELECT ROUND(AVG(rp.Nota), 1) AS "Promedio"
FROM Prueba p,
     Resultado_Prueba rp,
     Alumno a,
     Alumno_Curso ac,
     Persona pa,
     Curso c
WHERE c.nombre = 'programación'
  AND pa.rut = '50000000-1'
  AND a.id_persona = pa.id
  AND ac.id_alumno = a.id
  AND ac.id_curso = c.id
  AND rp.id_Alumno_Curso = ac.id;

-- alumnos y el promedio que tiene  en cada ram
SELECT concat(pa.nombres, ' ', pa.Apellido_paterno, ' ', pa.Apellido_materno) AS "Nombre Completo",
       c.nombre AS "Curso",
       ROUND(AVG(rp.Nota), 1) AS "Promedio"
FROM Prueba p,
     Resultado_Prueba rp,
     Alumno a,
     Alumno_Curso ac,
     Persona pa,
     Curso c
WHERE pa.id = a.id_persona
  AND a.id = ac.id_alumno
  AND c.id = ac.id_curso
  AND rp.id_Alumno_Curso = ac.id
GROUP BY pa.nombres,
         pa.Apellido_paterno,
         pa.Apellido_materno,
         c.nombre
ORDER BY pa.Apellido_paterno;

-- alumnos con más de un ramo con  promedio rojo
select j.nombres, j.apellido_p, j.apellido_m, count(j) as "cantidad promedio rojo"
from (
SELECT pa.id,
       pa.Nombres as nombres,
       pa.apellido_paterno as apellido_p,
       pa.apellido_materno as apellido_m,
       c.nombre AS curso,
       ROUND(AVG(rp.Nota), 1) AS promedio
FROM Prueba p,
     Resultado_Prueba rp,
     Alumno a,
     Alumno_Curso ac,
     Persona pa,
     Curso c
WHERE pa.id = a.id_persona
  AND a.id = ac.id_alumno
  AND c.id = ac.id_curso
  AND rp.id_Alumno_Curso = ac.id
GROUP BY pa.nombres,
         pa.apellido_paterno,
         pa.apellido_materno,
         pa.id,
         c.nombre,
         c.max_nota
HAVING ROUND(AVG(rp.Nota), 1) < (ROUND(c.Max_nota / 2))
) j
group by j.id, j.nombres, j.apellido_p, j.apellido_m
having count(j) > 1