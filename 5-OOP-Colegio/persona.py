# -*- coding: utf-8 -*-
from datetime import datetime


class Persona(object):
    def __init__(self, rut, nombres, apellido_p, apellido_m, email, telefono, fecha_n, fecha_i=datetime.now()):
        self.__RUT = rut
        self.__nombres = nombres
        self.__apellido_paterno = apellido_p
        self.__apellido_materno = apellido_m
        self.__email = email
        self.__telefono = telefono
        self.__fecha_nacimiento = fecha_n
        self.__fecha_ingreso = fecha_i

    # Getter of RUT
    @property
    def rut(self):
        return self.__RUT

    # Setter of RUT
    @rut.setter
    def rut(self, rut):
        self.__RUT = rut

    # Getter of nombres
    @property
    def nombres(self):
        return self.__nombres

    # Setter of nombres
    @nombres.setter
    def nombres(self, nombres):
        self.__nombres = nombres

    # Getter of apellido paterno
    @property
    def apellido_paterno(self):
        return self.__apellido_paterno

    # Setter of apellido paterno
    @apellido_paterno.setter
    def apellido_paterno(self, apellido_paterno):
        self.__apellido_paterno = apellido_paterno

    # Getter of apellido materno
    @property
    def apellido_materno(self):
        return self.__apellido_materno

    # Setter of apellido materno
    @apellido_materno.setter
    def apellido_materno(self, apellido_materno):
        self.__apellido_materno = apellido_materno

    # Getter of email
    @property
    def email(self):
        return self.__email

    # Setter of email
    @email.setter
    def email(self, email):
        self.__email = email

    # Getter of telefono
    @property
    def telefono(self):
        return self.__telefono

    # Setter of telefono
    @telefono.setter
    def telefono(self, telefono):
        self.__telefono = telefono

    # Getter of fecha nacimiento
    @property
    def fecha_nacimiento(self):
        return self.__fecha_nacimiento

    # Setter of fecha nacimiento
    @fecha_nacimiento.setter
    def fecha_nacimiento(self, fecha_n):
        self.__fecha_nacimiento = fecha_n

    # Getter of fecha ingreso
    @property
    def fecha_ingreso(self):
        return self.__fecha_ingreso

    # Setter of fecha ingreso
    @fecha_ingreso.setter
    def fecha_ingreso(self, fecha_i):
        self.__fecha_ingreso = fecha_i

    def __str__(self):
        return "{0} {1} {2}".format(self.nombres, self.apellido_materno, self.apellido_paterno)
