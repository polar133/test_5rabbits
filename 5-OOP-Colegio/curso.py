# -*- coding: utf-8 -*-
from datetime import datetime
import profesor
import prueba
import resultado


class Curso(object):
    def __init__(self, codigo, nombre, p_profesor, desc="", fecha_i=datetime.now(), max_nota=10):
        self.__codigo = codigo
        self.__nombre = nombre
        self.__descripcion = desc
        self.__fecha_inicio = fecha_i
        self.__max_nota = max_nota
        self.__profesor = p_profesor
        self.__profesor.asignar_curso(self)
        self.__alumnos = []
        self.__resultados_pruebas = []

    # Getter of codigo
    @property
    def codigo(self):
        return self.__codigo

    # Setter of codigo
    @codigo.setter
    def codigo(self, codigo):
        self.__codigo = codigo

    # Getter of nombre
    @property
    def nombre(self):
        return self.__nombre

    # Setter of nombre
    @nombre.setter
    def nombre(self, nombre):
        self.__nombre = nombre

    # Getter of descripcion
    @property
    def descripcion(self):
        return self.__descripcion

    # Setter of descripcion
    @descripcion.setter
    def descripcion(self, descripcion):
        self.__descripcion = descripcion

    # Getter of fecha_inicio
    @property
    def fecha_inicio(self):
        return self.__fecha_inicio

    # Setter of fecha_inicio
    @fecha_inicio.setter
    def fecha_inicio(self, fecha_inicio):
        self.__fecha_inicio = fecha_inicio

    # Getter of maxima nota
    @property
    def max_nota(self):
        return self.__max_nota

    # Setter of maxima nota
    @max_nota.setter
    def max_nota(self, max_nota):
        if max_nota > 0:
            self.__max_nota = max_nota

    # Getter of profesor
    @property
    def profesor(self):
        return self.__profesor

    # Setter of profesor
    @profesor.setter
    def profesor(self, p_profesor):
        if type(p_profesor) is profesor.Profesor:
            if self.__profesor.a_cargo_de_curso(self.codigo):
                self.__profesor.remover_curso(self.codigo)
            self.__profesor = p_profesor
            self.__profesor.asignar_curso(self)

    # Getter of resultados
    @property
    def pruebas(self):
        return self.__resultados_pruebas

    # Getter of alumnos
    @property
    def alumnos(self):
        return self.__alumnos

    def agregar_alumno(self, p_alumno):
        if p_alumno not in self.alumnos:
            self.alumnos.append(p_alumno)
            p_alumno.inscribir_curso(self)

    def retirar_alumno(self, p_alumno):
        if p_alumno in self.alumnos:
            self.alumnos.remove(p_alumno)
            p_alumno.retirar_curso(self)

    def agregar_resultado_prueba(self, p_alumno, p_prueba, nota, fecha=datetime.now()):
        if p_alumno in self.alumnos and type(p_prueba) is prueba.Prueba and nota <= self.max_nota:
            resultado_alumno = resultado.Resultado(p_alumno, self, p_prueba, nota, fecha)
            if resultado_alumno:
                self.__resultados_pruebas.append(resultado_alumno)

    def notas(self, p_alumno):
        return [resultado_alumno.nota for resultado_alumno in self.__resultados_pruebas if resultado_alumno.alumno is p_alumno]

    def promedio_alumnos(self):
        promedios = {}
        for alumno in self.alumnos:
            promedio_alumno = alumno.promedio(self)
            if promedio_alumno:
                promedios[alumno.rut] = promedio_alumno
        return promedios

    def promedios_rojos_alumnos(self):
        promedios = self.promedio_alumnos()
        bajo_promedio = {}
        for rut, nota in promedios.iteritems():
            if nota < (self.max_nota/2):
                bajo_promedio[rut] = nota
        return bajo_promedio

    def __str__(self):
        return self.nombre
