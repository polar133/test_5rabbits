from datetime import datetime
import alumno
import curso
import prueba


class Resultado(object):
    def __init__(self, p_alumno, p_curso, p_prueba, nota, fecha=datetime.now()):
        if type(p_alumno) is alumno.Alumno and type(p_curso) is curso.Curso and type(p_prueba) is prueba.Prueba:
            self.__alumno = p_alumno
            self.__curso = p_curso
            self.__prueba = p_prueba
            self.__nota = nota
            self.__fecha = fecha

    # Getter of alumno
    @property
    def alumno(self):
        return self.__alumno

    # Setter of alumno
    @alumno.setter
    def alumno(self, alumno):
        self.__alumno = alumno

    # Getter of curso
    @property
    def curso(self):
        return self.__curso

    # Setter of curso
    @curso.setter
    def curso(self, curso):
        self.__curso = curso

    # Getter of prueba
    @property
    def prueba(self):
        return self.__prueba

    # Setter of prueba
    @prueba.setter
    def prueba(self, prueba):
        self.__prueba = prueba

    # Getter of nota
    @property
    def nota(self):
        return self.__nota

    # Setter of nota
    @nota.setter
    def nota(self, nota):
        self.__nota = nota

    # Getter of fecha
    @property
    def fecha(self):
        return self.__fecha

    # Setter of fecha
    @fecha.setter
    def fecha(self, fecha):
        self.__fecha = fecha

    def __str__(self):
        return "Alumno: {0} Curso: {1} Prueba: {2} Nota: {3}"\
            .format(self.alumno, self.curso, self.prueba, self.nota)
